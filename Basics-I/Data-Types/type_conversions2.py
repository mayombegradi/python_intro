import datetime

today = datetime.date.today()
year = today.year

birth_year = input('What year were you born? ')
age = year - int(birth_year)
print(f'You are {age} years old')