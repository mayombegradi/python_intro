# Dictionary
import datetime
today = datetime.date.today()
year = today.year

location = {
    'country': 'France',
    'town': 'Nice'
}

# Keys are any datatypes that are IMMUTABLES like numbers, booleans, strings and so on
# Lists cannot be used as keys.
person = {
    'name': 'Gradie',
    'sex': 'male',
    'age': year - 1952,
    'location': location,
    'friends': ['Losanganya', 'Tandi', 'Mayambula']
}

print(person)
print(person['friends'])