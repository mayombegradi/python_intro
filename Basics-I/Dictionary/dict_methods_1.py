# Dictionary
location = {
    'country': 'France',
    'town': 'Nice'
}

person = {
    'name': 'Gradie',
    'sex': 'male',
    'location': location,
    'friends': ['Losanganya', 'Tandi', 'Mayambula']
}

print(person.get('name'))    # 'Gradie'
print(person.get('age'))     # None
print(person.get('age', 55)) # Default 55

# Create a dict via dict function.
user = dict(name='Junior', role='Admin')
print(user)
