user = {
    'username': 'joedo',
    'age': 23
}

print('size' in user) # False
print('age'  in user) # True

print('sex' in user.keys()) # False
print('age' in user.keys()) # True

print('joedo' in user.values()) # True

user1 = user.copy()
print(user1)

user.update({'age': 28})
user.update({'sex': 'male'}) # This will be added as it does not exist
print(user)

user.clear()
print(user)   # {}