# 2-D/multi-D arrays are critical in machine learning
# image processing.
matrix = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
]

print(matrix[1][2]) # 6