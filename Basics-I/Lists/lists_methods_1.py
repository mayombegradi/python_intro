basket = [1, 2, 3, 4, 5]
print(len(basket))

# ADDING ELEMENTS
# Append adds an element `in-place`.
# So, its return value is `None`.
new_basket = basket.append(6)
print(basket)
print(new_basket) # None

# list.insert(Index, Value) happens `in-place` as well.
new_basket = basket.insert(0, 0)
print(basket)
print(new_basket) # None

# list.extend([x, y, z]) appends the list to the existing one.
# Similar to the other methods, it's an `in-place` method.
new_basket = basket.extend([7, 8, 9])
print(basket)
print(new_basket) # None

# REMOVING ELEMENTS
basket = [1, 2, 3, 4, 5]
element = basket.pop() # 5
print(basket)  # [1, 2, 3, 4]
print(element) # 5

basket = [1, 2, 3, 4, 5]
# list.remove(element) removes the element, NOT THE INDEX!
element = basket.remove(4) # Removes the element 4 if it exists.
print(basket)  # [1, 2, 3, 5]
print(element) # None

basket = [1, 2, 3, 4, 5]
element = basket.pop(3) # 4
print(basket)  # [1, 2, 3, 5]
print(element) # 4

basket = [1, 2, 3, 4, 5]
basket.clear()
print(basket) # []