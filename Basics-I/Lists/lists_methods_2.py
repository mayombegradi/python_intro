basket = ['a', 'b', 'c', 'd', 'e']

# list.index(element)
print(basket.index('b'))
# list.index(element, start, stop)
# finds element between index start and stop.
# If the element does not exist, an error occurs.
print(basket.index('b', 0, 4))

# print(basket.index('b', 3, 4)), returns an error.

print('x' in basket) # False
print('d' in basket) # True

greet = 'hello'
print(greet.count('l')) # 2