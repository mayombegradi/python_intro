basket = ['a', 'e', 'c', 'd', 'b', 'd']
basket.sort() # in-place

print(basket) # ['a', 'b', 'c', 'd', 'd', 'e']

basket = ['a', 'e', 'c', 'd', 'b', 'd']
# Creates a new copy of the list and
# Keeps the old list intact.
sorted_list = sorted(basket)
print(sorted_list)
print(basket)

basket = ['a', 'e', 'c', 'd', 'b', 'd']
basket.reverse() # in-place
print(basket)

new_basket = basket[::-1] # Creates a new list and keeps unchanged the old one.
print(new_basket) # ['a', 'e', 'c', 'd', 'b', 'd']
print(basket)     # ['d', 'b', 'd', 'c', 'e', 'a']

print(list(range(10)))    # ['a', 'e', 'c', 'd', 'b', 'd']
print(list(range(1, 10))) # [1, 2, 3, 4, 5, 6, 7, 8, 9]

sentence = '-'
new_sentence = sentence.join(['Hi', 'Gradie', 'Mayombe'])
print(sentence) # '-'
print(new_sentence) # 'Hi-Gradie-Mayombe'