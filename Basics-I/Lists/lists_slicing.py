amazon_cart = [
    'notebooks',
    'sunglasses',
    'grapes',
    'toys'
]

print(amazon_cart[0:2])  # ['notebooks', 'sunglasses']
print(amazon_cart[0::2]) # ['notebooks', 'grapes']

amazon_cart[0] = 'laptop' # Lists are mutable!
print(amazon_cart) # ['laptop', 'sunglasses', 'grapes', 'toys']

new_cart = amazon_cart[0:2] # This creates a copy of the sublist.
print(new_cart)

# Both point to the same memory location.
new_cart = amazon_cart # Any change to one will impact the other one
