print(f'bin(168) => {bin(168)}') # 0b10101000
print(f'oct(168) => {oct(168)}') # 0o250
print(f'hex(168) => {hex(168)}') # 0xa8
print('')

print(f"int('0b10101000', 2) => {int('0b10101000', 2)}") # 168
print(f"int('0xa8', 16) => {int('0xa8', 16)}")      # 168
print(f"int('0o250', 8) => {int('0o250', 8)}")      # 168
print('')

print(f"bin(5)       => {bin(5)}")       # 0b101   - 05
print(f"bin(5 << 2)  => {bin(5 << 2)}")  # 0b10100 - 20 (Bitwise Shift Left)
print(f"bin(20 >> 2) => {bin(20 >> 2)}") # 0b101   - 05 (Bitwise Shift Roght)