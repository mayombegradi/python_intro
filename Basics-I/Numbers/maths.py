print(f'2 ** 3 = {2 ** 3}') # 2 raised to the pow 3 => 8
print(f'5 // 4 = {5 // 2}')   # integer division => 1

# math functions
print(f'round(1.08, 1) => {round(1.08, 1)}')
print(f'round(3.6) => {round(3.6)}')
print(f'abs(-200)  => {abs(-200)}')

# augmented assignment operator
some_value = 5
print(some_value)

some_value -= 1
print(some_value)

some_value += 1
print(some_value)

some_value *= 2
print(some_value)