print(f'4 / 2 = {4 / 2}')
print(f'4 * 2 = {4 * 2}')
print(f'4 + 2 = {4 + 2}')
print(f'4 - 2 = {4 - 2}')
print(f'4 % 2 = {4 % 2}')

print(f'type(5.0) => {type(5.0)}') # <class 'float'>
print(f'type(500) => {type(500)}') # <class 'int'>