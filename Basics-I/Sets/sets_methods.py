my_set = set(list(range(1, 6)))
your_set = set(list(range(4, 11)))

print(my_set.difference(your_set))   # {1, 2, 3}
print(your_set.difference(my_set))   # {6, 7, 8, 9, 10}
print(my_set.intersection(your_set)) # {4, 5}

new_set = my_set.union(your_set) # same as: my_set | your_set
print(new_set)

my_set.discard(5)
print(my_set) # {1, 2, 3, 4}

my_set.add(5)
print(my_set)

print(my_set.issubset(your_set))
print(your_set.issubset(my_set))