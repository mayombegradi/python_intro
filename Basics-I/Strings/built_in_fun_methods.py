greet = 'hello'

# function are invoked in the format fun(Element).
# E.g: len(string)
print(len(greet))
print(greet[0:len(greet)]) # hello

# Methods are applied to elements.
# E.g: string.upper()

quote = 'to be or not to be'
print(quote.upper())       # TO BE OR NOT TO BE
print(quote.lower())       # to be or not to be
print(quote.capitalize())  # To be or not to be

print(quote.find('be'))          # 3
print(quote.replace('be', 'me')) # to me or not to me
print(quote)                     # to be or not to be -> Strings are immutable!

print('l' in greet) # True
print('w' in greet) # False