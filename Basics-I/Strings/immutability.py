selfish = '01234567'

# selfish[7] = 8 is illegal

# The line below kills the first variable selfish and
# it's replaced by a new one!
selfish = selfish + '8'