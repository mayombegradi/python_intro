username = input('Username: ')
password = input('Password: ')

masked_password = '*' * len(password)
print(f'{username}, your password: {masked_password}')