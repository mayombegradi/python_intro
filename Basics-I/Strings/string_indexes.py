#       012345
name = 'GRADIE'

print(name[0]) # G
print(name[5]) # E

# IndexError: string index out of range
# print(name[9])

# string[start_included:stop_excluded]
print(name[0:6]) # GRADIE - Stop at 5th element
print(name[0:5]) # GRADI  - Stop at 4th element

# string[start:stop:stepover]
print(name[0:6:2]) # GAI

# string[start:] From starting index to the end
print(name[0:]) # GRADIE

# string[:stop] Starts from 0 to stop index excluded
print(name[:5]) # GRADI

# string[::stepover]
print(name[::1]) # GRADIE

# string[-1] Starts counting from the end of the string
print(name[-1]) # E

# string[::-1] Reverses the string
print(name[::-1])