# Tuples are immutable and more efficient as compared to lists.
my_tuple = (1, 2, 3, 4, 5)

print(my_tuple[1]) # This points at the index 1 => 2.
print(5 in my_tuple) # True
print(0 in my_tuple) # False