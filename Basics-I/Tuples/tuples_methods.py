my_tuple = (1, 2, 3, 4, 5, 4)
new_tuple = my_tuple[1:2]

print(len(my_tuple))
print(my_tuple.count(4)) # 2
print(my_tuple.index(3)) # 3
print(new_tuple) # (2,)

x, y, z, *other = (1, 2, 3, 4, 5)
print(other) # [4, 5, 4]