# snake_case
# Start with lowercase or underscore
# Contains alphanumerics and underscore
# Case sensitive
# Don't use keywords

full_name = 'Gradie Mayombe' # snake_case
score1 = 17 # alphanumerics

# `full_name` better than `fn`

# By convention, constants are written in capital
PI = 3.14

# Multi-assignment.
name, age, sex = 'Gradie Mayombe', 29, 'male'
print(name)
print(age)
print(sex)