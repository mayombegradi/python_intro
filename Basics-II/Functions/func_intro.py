picture = [
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 1, 1, 0, 0],
    [0, 1, 1, 1, 1, 1, 0],
    [1, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0]
]

def show_tree(matrix):
    for outer_item in matrix:
        for inner_item in outer_item:
            if inner_item == 0:
                print(' ', end=' ')
            else:
                print('*', end=' ')
        print()

show_tree(picture)