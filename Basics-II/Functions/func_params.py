# Default arguments
def say_hello(name='Anonymous', emoji='😅'):
    print(f'Hello {name} {emoji}')

# Positional arguments
say_hello('Gradie', '😍')

# Keyword arguments
# This is bad practice!
say_hello(emoji='😍', name='Gradie')

say_hello()