print([] == 1)      # False
print('' == 1)      # False
print([] == [])     # True
print(True == 1)    # True
print(10 == 10.0)   # True

print('-----')

print([] is 1)      # False
print('' is 1)      # False
print([] is [])     # False
print(True is 1)    # False
print(10 is 10.0)   # False