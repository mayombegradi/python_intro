# >, <, >=, <=, !=, == and not()
print(4 == 5)
print(0 != 0)
print(not(True))
print(not('He')) # Applies Truthy.
print('a' >= 'A')