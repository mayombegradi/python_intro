for index, char in enumerate('Dam'):
    print(index, char)

print()

for index, value in enumerate([1, 2, 3]):
    print(index, value)

print()

for index, value in enumerate((1, 2, 3)):
    print(index, value)

print()