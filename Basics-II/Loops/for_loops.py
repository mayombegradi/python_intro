for character in 'Hotel Tava': # Over a string
    print(character)

print()

for number in (1, 2, 3, 4, 5): # Over a tuple
    print(number)

print()

for number in [1, 2, 3, 4, 5]: # Over a list
    print(number)

print()

for number in range(1, 6): # Over a range
    print(number)