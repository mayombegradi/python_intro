user = {
    'name': 'Golem',
    'age': 27,
    'abilities': ['swim']
}

for item in user.items():
    print(item)

print()

for key, value in user.items():
    print(key, value)

print()

for item in user.values():
    print(item)

print()

for item in user.keys():
    print(item)