picture = [
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 1, 1, 0, 0],
    [0, 1, 1, 1, 1, 1, 0],
    [1, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0]
]

for outer_item in picture:
    for inner_item in outer_item:
        if inner_item == 0:
            print(' ', end=' ')
        else:
            print('*', end=' ')
    print()

#       *
#     * * *
#   * * * * *
# * * * * * * *
#       *
#       *