is_friend = True
is_user = True

# and uses a short-circuit. If the first condition is false, it stops there.
# or on the other hand would still check the other condition in case it's true.
if is_friend or is_user:
    print('Best friends forever')