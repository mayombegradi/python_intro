is_friend = True
# condition_if_true if condition else condition_else
can_message = "Message allowed" if is_friend else "Message not allowed"

print(can_message)