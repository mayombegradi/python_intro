print(bool(0))     # False
print(bool(''))    # False
print(bool({}))    # False
print(bool([]))    # False
print(bool(0.0))   # False
print(bool(None))  # False
print(bool(set())) # False

username, password = 'johny', '123'

if username and password:
    print(f'Welcome {username}')